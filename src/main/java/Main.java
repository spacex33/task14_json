import comparator.TourComparator;
import model.Tour;
import org.everit.json.schema.ValidationException;
import parser.Parser;
import parser.json_parser.JacksonParser;
import validation.json_validator.JSONValidator;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        Parser<List<Tour>> parser = new JacksonParser();
        File json = new File("src\\main\\resources\\tours.json");
        File jsonSchema = new File("src\\main\\resources\\toursSchema.json");
        try {
            JSONValidator.validateBySchema(json, jsonSchema);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        List<Tour> tours = parser.parse(json);
        tours.sort(new TourComparator());
        System.out.println(tours);
    }
}
