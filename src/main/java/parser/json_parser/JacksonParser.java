package parser.json_parser;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import model.Hotel;
import model.Tour;
import parser.Parser;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class JacksonParser implements Parser<List<Tour>> {

    private static final String MODEL_CLASSES_PATH = "model";

    @Override
    public List<Tour> parse(File file) throws IOException {
        JsonParser jsonParser = new JsonFactory().createParser(file);
        List<Tour> tours = new ArrayList<>();

        // skip several tokens,
        // that represents the beginning of the array and the first object
        jsonParser.nextToken();
        jsonParser.nextToken();

        // until the main array inside json is not finished, we continue to parse the objects
        while (!JsonToken.END_ARRAY.equals(jsonParser.currentToken())) {
            Tour tour = new Tour();
            parseObject(jsonParser, tour);

            // skip token, that represents end of object
            jsonParser.nextToken();
            tours.add(tour);
        }
        jsonParser.close();
        return tours;
    }

    private Object parseObject(JsonParser jsonParser, Object expectedObject) throws IOException {
        jsonParser.nextToken();
        while (!JsonToken.END_OBJECT.equals(jsonParser.currentToken())) {
            String key = null;
            Object value = null;

            // here we take the key of json object
            if (JsonToken.FIELD_NAME.equals(jsonParser.currentToken())) {
                key = jsonParser.getCurrentName();
            }
            jsonParser.nextToken(); // step to another token

            // here we get the value of json object
            switch (jsonParser.currentToken()) {
                case START_OBJECT: // value can be represented as object
                    // parse the object inside json object recursively
                    value = parseObject(jsonParser, findObjectByName(key));
                    break;
                default:
                    // by default we get the value as String
                    value = jsonParser.getValueAsString();
                    break;
            }
            // assign the obtained values to the object through its methods
            setValuesByMethod(expectedObject, key, value);
            jsonParser.nextToken();
        }
        return expectedObject;
    }

    private void setValuesByMethod(Object object, String methodName, Object methodValue) {
        try {
            // construct the setter from the method name
            String setterName = "set" + methodName.substring(0, 1).toUpperCase() + methodName.substring(1);

            if (methodValue instanceof String) {
                invokeMethodWithStrParam(object, setterName, (String) methodValue);
            } else {
                invokeMethodWithObjParam(object, setterName, methodValue);
            }
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
        return true;
    }

    private void invokeMethodWithStrParam(Object object, String setterName, String methodValue) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method setter;
        if (isInteger(methodValue)) {
            int val = Integer.parseInt(methodValue);
            setter = object.getClass().getMethod(setterName, int.class);
            setter.invoke(object, val);
        } else if ("true".equals(methodValue) || "false".equals(methodValue)) {
            boolean val = Boolean.parseBoolean(methodValue);
            setter = object.getClass().getMethod(setterName, boolean.class);
            setter.invoke(object, val);
        } else {
            setter = object.getClass().getMethod(setterName, (methodValue).getClass());
            setter.invoke(object, methodValue);
        }
    }

    private void invokeMethodWithObjParam(Object object, String setterName, Object methodValue) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        Method setter = object.getClass().getMethod(setterName, methodValue.getClass());
        setter.invoke(object, (Hotel) methodValue);
    }

    private Object findObjectByName(String name) {
        Object obj = null;
        try {
            obj = Class.forName(MODEL_CLASSES_PATH
                    + "."
                    + name.substring(0, 1).toUpperCase()
                    + name.substring(1)).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return obj;
    }

}
