package comparator;

import model.Tour;

import java.util.Comparator;

public class TourComparator implements Comparator<Tour> {

    @Override
    public int compare(Tour tour1, Tour tour2) {
        return Integer.compare(tour1.getTourId(), tour2.getTourId());
    }
}
