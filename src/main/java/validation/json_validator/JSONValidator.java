package validation.json_validator;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class JSONValidator {

    public static void validateBySchema(File jsonFile, File jsonSchemaFile) throws FileNotFoundException, ValidationException {
        JSONObject jsonSchema = new JSONObject(new JSONTokener(new FileInputStream(jsonSchemaFile)));
        JSONArray jsonSubject = new JSONArray(new JSONTokener(new FileInputStream(jsonFile)));

        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(jsonSubject);
    }
}
